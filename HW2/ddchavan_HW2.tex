\documentclass[a4paper,fleqn]{article}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{amsthm}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{minted}
\usepackage{caption}

\usepackage[xetex]{graphicx} 
\usepackage{fontspec,xunicode} 
\usepackage[pdfauthor={Devendra D. Chavan}, 
                        pdftitle={Homework 2 - CSC591 - Graph Data Mining}, 
                        pdfsubject={Homework 2}, 
                        pdfkeywords={CSC591, NCSU, Graph Data Mining, Homework}, 
                        pdfproducer={XeLateX with hyperref} 
                        pdfcreator={Xelatex}]{hyperref} 
                
\begin{document}
\section*{\centering Graph Data Mining - Homework 2}
\vspace{10 pt}
\textbf{Name : Devendra D. Chavan}\\
\textbf{Unity Id : ddchavan}

\section*{Problem 1}
\begin{enumerate}[(1)]
    \item
    {
        Since it is a MIN-HEAP, the root node is the node with the smallest 
        degree. It can accessed in constant time i.e. $O(1)$.
    }
    \item
    {
        The maximum element in a MIN-HEAP could be at any of the leaf level 
        nodes. Therefore, one would need to traverse both the left and right 
        sub trees for each node to determine the maximum amongst the leaves 
        (there are a total of $\frac{\displaystyle N}{\displaystyle 2}$ leaves
        in a complete binary tree) i.e. a complexity of $O(N)$
    }
    \item
    {
        \begin{enumerate}[(a)]
        \item
        {     
            The STEP operation defines a single step of the simulation on a 
            MIN-HEAP represented in an array $A$ and associated with a graph
            $G = (V, E)$ represented as an adjacency list.\\
            \textbf{Standard methods on a MIN-HEAP}
            \begin{enumerate}
            \item
            {
                The EXTRACT-MIN operation is to remove the root element in a 
                MIN-HEAP. 
            }
            \item
            {
                NEIGHBORS($index$) returns the list of neighbors in an 
                adjacency list for the vertex having the specified $index$.
            }
            \end{enumerate}
            \begin{algorithm}
                \label{alg:SimulationStep}
                \caption{Remove vertex with the highest degree and update 
                neighbors in MIN-HEAP ($K$ times), STEP(A)}
                \begin{algorithmic}[1]
                \State $max\_index \gets \lfloor \frac{\displaystyle A.length}
                {\displaystyle 2}\rfloor$
                \State $max \gets A[max\_index]$ 
                \For{$i = max\_index \to A.length$}
                    \Comment{Search the leaves for the max element}
                    \If{$A[i] > max$}
                        \State $max \gets A[i]$
                        \State $max\_index \gets i$
                    \EndIf
                \EndFor
                \State \Call{DECREASE-KEY}{$A$, $max\_index$, -$\infty$}
                    \Comment{Decrease the max value so that it is at the root}
                \State $immunized\_index \gets$ \Call{EXTRACT-MIN}{$A$}
                \ForAll{$i \in NEIGHBOURS(immunized\_index)$} 
                    \State \Call{DECREASE-KEY}{$A$, $i$, $deg(i) - 1$}
                \EndFor
                \end{algorithmic}
            \end{algorithm}
            \begin{algorithm}
                \label{alg:Decrease-Key}
                \caption{Decrease the key of a MIN-HEAP, 
                DECREASE-KEY($A$, $i$, $key$)}
                \begin{algorithmic}[1]
                    \State $A \gets key$
                    \While{$i > 1 \text{ and } A[PARENT(i)] > A[i]$}
                        \State \Call{SWAP}{$A[i]$, $A[PARENT(i)]$}
                        \State $i \gets$\Call{PARENT}{i}
                    \EndWhile
                \end{algorithmic}
            \end{algorithm}
        }
        \item
        {
            \textbf{Assumptions}
            \begin{enumerate}
                \item
                {
                    The graph is represented as an adjacency list.
                }
            \end{enumerate}
        }
        \item
        {
            The time taken for each step can be analyzed as follows:
            \begin{enumerate}
            \item
            {
                Searching for the max element at the leaf level (Alg1 : lines 
                3-8): 
                $O(\frac{\displaystyle N}{\displaystyle 2}) = O(N)$
            }
            \item            
            {
                Decreasing the key of the max node - DECREASE-KEY (Alg1: line 
                9): $O(\lg N)$                
            }
            \item
            {
                Removing the minimum (root) from the MIN-HEAP (Alg1: line 10):
                $O(\lg N)$
            }
            \item
            {
                Finding the degree of each vertex, $u$ (Alg1: line 12): 
                $O(N)$\\
                Calling DECREASE-KEY (Alg1: line 12): $O(\lg N)$\\
                Iterating over all neighbors of the max degree vertex, $v$ 
                (Alg1: lines 11-13):
                $O(N) \times O(N) \times O(\lg N) = O(N^2\lg N)$                
            }
            \item
            {
                \textbf{Total cost for each step} = $O(N) + O(\lg N) + 
                O(N^2\lg N) = O(N^2\lg N)$.
            }
            \end{enumerate}
            Therefore, \textbf{total cost for the entire simulation} (each 
            step run $K$ times) = $K \times O(N^2\lg N) = O(N^2\lg N)$ (since 
            $K<<N$).
        }
        \item
        {
            \textbf{Pros}
            \begin{enumerate}
                \item                
                {
                    The entire simulation can be run in polynomial time.
                }
            \end{enumerate}
            \textbf{Cons}
            \begin{enumerate}
                \item
                {
                    Decreasing the key of each item results in the 
                    recalculation of the degree of each neighbour of that item.
                    i.e. the degrees are not cached.
                }
            \end{enumerate}
        }
        \end{enumerate}
    }
\end{enumerate}

\section*{Problem 2}
\begin{enumerate}[(1)]
    \item
    {
        Assuming that the graph is given in the form of an edge list, then 
        finding the vertex with the highest degree would mean iterating over 
        all the elements in the list i.e. $O(|E|) = O(N^2)$ (in the worst 
        case) (where $|E|$ is the number of edges in the graph and $N$ is the 
        number of vertices).
    }
    \item
    {
        In case of an adjacency list representation of a graph, finding the 
        degree of each vertex requires $O(deg(v))$. To find the maximum
        amongst all the vertices would require iterating over all of them, i.e.
        thus the time complexity to find the vertex with the highest degree 
        would be $O(|V|deg(v))$ which in the worst case (fully connected graph) 
        would be $O(N^2)$ where $N$ is the number of vertices.\\
        The next step would be to remove the vertex $v$ from the graph, the 
        time taken to remove a vertex is of the order $O(deg(v))$ which in the
        worst case would be $O(N-1) = O(N)$ and update the degrees of the 
        neighbors i.e. $O(N^2)$. This would have to repeated for 
        all the $N$ vertices i.e. $O(N^4)$.\\
        Therefore the total cost would be $O(N^2) + O(N^4) = O(N^4)$.
    }
    \item
    {
        In case a MIN-HEAP, the operations would be similar to those stated in 
        Problem 1.3. The difference being that the STEP operation would be 
        repeated for all the $N$ nodes i.e. the total cost would be $N \times
        O(N^2 \lg N) = O(N^3 \lg N)$. In this case, since all the nodes are 
        being eventually removed the cost of performing all the steps now has a 
        significant multiplier $N$ assosciated with it, instead of a small 
        value $K$.
    }
    \item
    {
        Since the degrees are from a limited range ($1..N-1$), they can be 
        sorted using a $CountingSort$ in $O(N)$ i.e. COUNTINGSORT($A$, $B$, 
        $N-1$)
        \begin{algorithm}
            \label{alg:CountingSort}
            \caption{Sorts an array $A$ containing integers in the range $0$ to
            $k$ and returns sorted array $B$, COUNTINGSORT($A$, $B$, $k$)}
            \begin{algorithmic}[1]
                \State Let $C[0..k]$ be a new array
                \Comment{As the degree of vertex is always greater than 1, 
                $C[0] = 0$ always}
                \For{$i = 0 \to k$}
                    \State $C[i] \gets 0$
                \EndFor
                \For{$j = 1 \to A.length$}
                    \State $C[A[j]] \gets C[A[j]]+1$
                \EndFor
                \For{$i = 1 \to k$}
                    \State $C[i] \gets C[i] + C[i-1]$
                \EndFor
                \For{$j = A.length \textbf{ downto } 1$}
                    \State $B[C[A[j]]] \gets A[j]$
                    \State $C[A[j]] \gets C[A[j]] - 1$
                \EndFor
            \end{algorithmic}
        \end{algorithm}
    }
\end{enumerate}

\end{document}
