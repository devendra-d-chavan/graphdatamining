> # Executed in RStudio 0.97.551 with R 3.0.1
> library("kernlab")
> library("ggplot2")
> library("reshape")
> 
> # ------ Exercise 2 ------ 
> 
> GenerateDataset <- function(n, mean_ = 3, sigma = 1)
+ {  
+   n1 <- round(n/2)
+   n2 <- n - n1
+   
+   radial_sep = 6
+   
+   r <- rnorm(n1, mean = mean_, sd = sigma)
+   a <- 2*pi*rnorm(n1, mean = mean_, sd = sigma)
+   a1 <- r*sin(a)
+   a2 <- r*cos(a)  
+   r <- radial_sep + rnorm(n2, mean = mean_, sd = sigma)
+   a <- 2*pi*rnorm(n2, mean = mean_, sd = sigma)
+   b1 <- r*sin(a)
+   b2 <- r*cos(a)
+   x <- rbind(cbind(a1,a2), cbind(b1,b2))
+   y <- c(rep(1,n1), rep(2,n2))
+   dataset <- as.data.frame(cbind(x, y))
+   names(dataset) <- c("x", "y", "class")      
+   
+   return(dataset)
+ }
> 
> PlotDataset <- function(df)
+ {
+   ggplot(df, aes(x=x, y = y, col=ifelse(class == 1, "red", "blue"))) + 
+     geom_point(size=3) +     
+     theme(legend.position="none") +
+     xlab("x") + 
+     ylab("y")
+ }
> 
> df <- GenerateDataset(n = 200)
> PlotDataset(df)
> 
> # ------ Exercise 3 ------ 
> 
> GetConfusionMatrix <- function(found_df, ground_truth_df)
+ {
+   true_positive <- 0
+   false_positive <- 0
+   false_negative <- 0
+   true_negative <- 0
+   
+   found_class_index <- which(colnames(found_df) == "class")  
+   ground_truth_class_index <- which(colnames(ground_truth_df) == "class")  
+   
+   all_rows <- rownames(ground_truth_df)
+   
+   for(i in levels(as.factor(ground_truth_df[,ground_truth_class_index])))
+   {
+     ground_truth_rows <- rownames(ground_truth_df[which(ground_truth_df[, ground_truth_class_index] == i),])
+     found_rows <- rownames(found_df[which(found_df[, found_class_index] == i),])
+     
+     true_positive <- true_positive + length(intersect(ground_truth_rows, found_rows))
+     false_positive <- false_positive + length(setdiff(found_rows, ground_truth_rows))
+     false_negative <- false_negative + length(setdiff(ground_truth_rows, found_rows))
+     true_negative <- true_negative + length(intersect(setdiff(all_rows, ground_truth_rows),
+                                                       setdiff(all_rows, found_rows)))
+   }
+   
+   return(matrix(c(true_positive, false_positive, false_negative, true_negative), 
+                 nrow = 2, ncol = 2))
+ }
> 
> GetPerformanceMetrics <- function(confusion_matrix)
+ {
+   true_positive <- confusion_matrix[1,1]
+   false_positive <- confusion_matrix[1,2]
+   false_negative <- confusion_matrix[2,1]
+   true_negative <- confusion_matrix[2,2]
+   
+   
+   recall <- true_positive/(true_positive+false_negative)                              
+   precision <- true_positive/(true_positive+false_positive)                           
+   f_measure <- (2*precision*recall)/(precision+recall)                              
+   specificity <- true_negative/(true_negative+false_positive)                         
+   rand_index <- (true_positive+true_negative)/(true_positive+false_positive+
+                                                  false_negative+true_negative)
+   
+   metrics_list <- list("recall" = recall, "precision" = precision, 
+                         "f_measure" = f_measure, "specificity" = specificity, 
+                         "rand_index" = rand_index)  
+   
+   return(metrics_list)
+ }
> 
> PrintPerformanceMetrics <- function(metrics_list)
+ { 
+   print("Recall : ")
+   print(as.numeric(unlist(metrics_list["recall"])))
+   
+   print("Precision : ")
+   print(as.numeric(unlist(metrics_list["precision"])))
+   
+   print("F Measure : ")
+   print(as.numeric(unlist(metrics_list["f_measure"])))
+   
+   print("Specificity : ")
+   print(as.numeric(unlist(metrics_list["specificity"])))
+   
+   print("RAND Index : ")
+   print(as.numeric(unlist(metrics_list["rand_index"])))
+ }
> 
> GetKMeansClusters <- function(df, centers = 2)
+ {
+   k <- kmeans(df, centers)
+   ret_df <- data.frame(df)
+   ret_df$class <- factor(k$cluster)
+     
+   return(ret_df)
+ }
> 
> class_index <- which(colnames(df) == "class")  
> kmeans_df <- GetKMeansClusters(df[,-class_index])
> confusion_matrix <- GetConfusionMatrix(found_df=kmeans_df, ground_truth_df=df)
> metrics_list <- GetPerformanceMetrics(confusion_matrix)
> PrintPerformanceMetrics(metrics_list)
[1] "Recall : "
[1] 0.425
[1] "Precision : "
[1] 0.425
[1] "F Measure : "
[1] 0.425
[1] "Specificity : "
[1] 0.425
[1] "RAND Index : "
[1] 0.425
> 
> GetCrossValidatedSVMMetrics <- function(df, folds = 10, kernel = "vanilladot",
+                                         sigma = 0.1, cost = 1)
+ { 
+   class_index = which(colnames(df) == "class")
+   
+   confusion_matrix <- matrix(0, nrow = 2, ncol = 2)
+   cross_error <- 0
+   percent_sv <- 0
+   num_rep <- 5
+   
+   for(i in seq(1, num_rep))
+   {
+     mat <- as.matrix(df[sample(nrow(df)),])
+     test_indices <- split(sample(nrow(df)), rep(1:folds))
+     
+     for(test_index in test_indices)
+     {
+       #test_index = test_indices[1]
+       test_index = as.numeric(unlist(test_index))
+       
+       # Divide into test and training data sets
+       mat_train <- mat[-test_index, -class_index]
+       mat_test <- mat[test_index, -class_index]
+       class_train <- mat[-test_index, class_index]
+       class_test <- mat[test_index, class_index]
+       
+       if(kernel == "rbfdot" || kernel == "anovadot" || kernel == "laplacedot")
+       {
+         k <- ksvm(mat[-test_index, -class_index], mat[-test_index, class_index], 
+                   type="C-svc", kernel=kernel, C=cost, 
+                   kpar = list(sigma = sigma), 
+                   scaled=c(), cross = 10)
+       }
+       else
+       {
+         k <- ksvm(mat[-test_index, -class_index], mat[-test_index, class_index], 
+                   type="C-svc", kernel=kernel, C=cost, scaled=c(), cross = 10)
+       }
+       
+       cross_error <- cross_error + cross(k)
+       percent_sv <- percent_sv + (nSV(k))/(nrow(mat) - length(test_index))
+       class_actual <- predict(k, mat[test_index, -class_index])
+       
+       found_df <- as.data.frame(cbind(mat[test_index,-class_index], 
+                                       class = class_actual))
+       ground_truth_df <- as.data.frame(mat[test_index,])
+       
+       confusion_matrix <- confusion_matrix + GetConfusionMatrix(found_df, 
+                                                                 ground_truth_df)
+     }
+   }
+   
+   confusion_matrix <- confusion_matrix/(num_rep * folds)
+   cross_error <- cross_error/(num_rep * folds)
+   percent_sv <- percent_sv/(num_rep * folds)
+   
+   metrics_list <- GetPerformanceMetrics(confusion_matrix)
+   metrics_list <- c(metrics_list, 
+                     "cross_error" = cross_error, 
+                     "fraction(support_vectors)" = percent_sv)  
+   
+   return(metrics_list)
+ }
> 
> folds <- 10
> metrics_list <- GetCrossValidatedSVMMetrics(df, folds)
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
 Setting default kernel parameters  
> PrintPerformanceMetrics(metrics_list)
[1] "Recall : "
[1] 0.666
[1] "Precision : "
[1] 0.666
[1] "F Measure : "
[1] 0.666
[1] "Specificity : "
[1] 0.666
[1] "RAND Index : "
[1] 0.666
> print("Cross error : ")
[1] "Cross error : "
> print(as.numeric(unlist(metrics_list["cross_error"])))
[1] 0.34
> 
> print("Fraction (Support vectors) : ")
[1] "Fraction (Support vectors) : "
> print(as.numeric(unlist(metrics_list["fraction(support_vectors)"])))
[1] 0.9053333
> 
> PlotPCA <- function(df)
+ {
+   test <- sample(1:nrow(df), 20)
+   
+   class_index = which(colnames(df) == "class")
+   
+   pca <- princomp(df[-test,-class_index], center=TRUE)
+   plot(pca)
+   loadings(pca)
+   summary(pca) 
+   P <- pca$scores 
+   p_df <- cbind(as.data.frame(P), df[-test, class_index])
+   colnames(p_df) <- c("x", "y", "class")
+   
+   emb <- predict(pca, as.matrix(df[test, -class_index]))
+   emb_df <- cbind(as.data.frame(emb), df[test, class_index])
+   colnames(emb_df) <- c("x", "y", "class")
+   
+   ggplot(p_df, aes(x = x, y = x, col = ifelse(class == 1, "red", "blue"))) +
+     geom_point(emb_df, mapping = aes(x = x, y = y, col = ifelse(class == 1, "red", "blue"))) +
+     geom_point(size=3) +
+     theme(legend.position="none") +
+     xlab("1st Principal Component") + 
+     ylab("2nd Principal Component")
+ }
> 
> PlotPCA(df)
> 
> # ------ Exercise 4 ------ 
> 
> GetKKMeansClusters <- function(df, kernel = "vanilladot", sigma)
+ {  
+   if(kernel == "rbfdot" || kernel == "anovadot" || kernel == "laplacedot")
+   {
+     k <- kkmeans(as.matrix(df), ncol(df), kernel=kernel, kpar = list(sigma = sigma))  
+   }
+   else
+   {
+     k <- kkmeans(as.matrix(df), ncol(df), kernel=kernel)
+   }
+   
+   found_df <- cbind(df, class = as.matrix(k))
+   return(found_df)
+ }
> 
> GetKKMeansPerformanceMetrics <- function(df, kernel = "vanilladot", sigma_values)
+ {  
+   res <- matrix(nrow = 0, ncol = 6)
+   class_index <- which(colnames(df) == "class")  
+   
+   for(sigma_value in sigma_values)
+   {
+     kkmeans_df <- GetKKMeansClusters(df[,-class_index], kernel = kernel, sigma_value)
+     confusion_matrix <- GetConfusionMatrix(found_df=kkmeans_df, ground_truth_df=df)
+     metrics_list <- GetPerformanceMetrics(confusion_matrix)
+     
+     res <- rbind(res, c(sigma_value,
+                          as.numeric(unlist(metrics_list["recall"])), 
+                          as.numeric(unlist(metrics_list["precision"])), 
+                          as.numeric(unlist(metrics_list["f_measure"])), 
+                          as.numeric(unlist(metrics_list["specificity"])),
+                          as.numeric(unlist(metrics_list["rand_index"]))))
+   }
+   
+   colnames(res) <- c("sigma", "recall", "precision", "f_measure", "specificity",
+                      "rand_index")
+   return(as.data.frame(res))
+ }
> 
> PlotSigmaMetrics <- function(metrics)
+ {
+   metrics.melt <- melt(metrics, id.vars="sigma")
+   ggplot(metrics.melt, aes(x=sigma, y=value, color=variable)) + geom_line()
+ }
> 
> PlotCostMetrics <- function(metrics)
+ {
+   metrics.melt <- melt(metrics, id.vars="cost")
+   ggplot(metrics.melt, aes(x=cost, y=value, color=variable)) + geom_line()
+ }
> 
> sigma_values <- seq(from = 0.1, to = 3.0, by=0.1)
> laplace_metrics <- GetKKMeansPerformanceMetrics(df, kernel = "laplacedot", 
+                                                 sigma_values)
> anova_metrics <- GetKKMeansPerformanceMetrics(df, kernel = "anovadot", 
+                                               sigma_values)
> rbf_metrics <- GetKKMeansPerformanceMetrics(df, kernel = "rbfdot", 
+                                             sigma_values)
> 
> print(laplace_metrics)
   sigma recall precision f_measure specificity rand_index
1    0.1  0.460     0.460     0.460       0.460      0.460
2    0.2  1.000     1.000     1.000       1.000      1.000
3    0.3  1.000     1.000     1.000       1.000      1.000
4    0.4  0.135     0.135     0.135       0.135      0.135
5    0.5  0.000     0.000       NaN       0.000      0.000
6    0.6  0.650     0.650     0.650       0.650      0.650
7    0.7  0.860     0.860     0.860       0.860      0.860
8    0.8  0.920     0.920     0.920       0.920      0.920
9    0.9  0.195     0.195     0.195       0.195      0.195
10   1.0  0.760     0.760     0.760       0.760      0.760
11   1.1  0.700     0.700     0.700       0.700      0.700
12   1.2  0.490     0.490     0.490       0.490      0.490
13   1.3  0.060     0.060     0.060       0.060      0.060
14   1.4  0.730     0.730     0.730       0.730      0.730
15   1.5  0.515     0.515     0.515       0.515      0.515
16   1.6  0.540     0.540     0.540       0.540      0.540
17   1.7  0.780     0.780     0.780       0.780      0.780
18   1.8  0.520     0.520     0.520       0.520      0.520
19   1.9  0.470     0.470     0.470       0.470      0.470
20   2.0  0.540     0.540     0.540       0.540      0.540
21   2.1  0.555     0.555     0.555       0.555      0.555
22   2.2  0.495     0.495     0.495       0.495      0.495
23   2.3  0.445     0.445     0.445       0.445      0.445
24   2.4  0.540     0.540     0.540       0.540      0.540
25   2.5  0.475     0.475     0.475       0.475      0.475
26   2.6  0.485     0.485     0.485       0.485      0.485
27   2.7  0.490     0.490     0.490       0.490      0.490
28   2.8  0.560     0.560     0.560       0.560      0.560
29   2.9  0.560     0.560     0.560       0.560      0.560
30   3.0  0.480     0.480     0.480       0.480      0.480
> print(anova_metrics)
   sigma recall precision f_measure specificity rand_index
1    0.1  0.100     0.100     0.100       0.100      0.100
2    0.2  0.575     0.575     0.575       0.575      0.575
3    0.3  0.715     0.715     0.715       0.715      0.715
4    0.4  0.205     0.205     0.205       0.205      0.205
5    0.5  0.225     0.225     0.225       0.225      0.225
6    0.6  0.745     0.745     0.745       0.745      0.745
7    0.7  0.730     0.730     0.730       0.730      0.730
8    0.8  0.375     0.375     0.375       0.375      0.375
9    0.9  0.710     0.710     0.710       0.710      0.710
10   1.0  0.560     0.560     0.560       0.560      0.560
11   1.1  0.615     0.615     0.615       0.615      0.615
12   1.2  0.585     0.585     0.585       0.585      0.585
13   1.3  0.745     0.745     0.745       0.745      0.745
14   1.4  0.385     0.385     0.385       0.385      0.385
15   1.5  0.280     0.280     0.280       0.280      0.280
16   1.6  0.690     0.690     0.690       0.690      0.690
17   1.7  0.640     0.640     0.640       0.640      0.640
18   1.8  0.320     0.320     0.320       0.320      0.320
19   1.9  0.640     0.640     0.640       0.640      0.640
20   2.0  0.520     0.520     0.520       0.520      0.520
21   2.1  0.290     0.290     0.290       0.290      0.290
22   2.2  0.410     0.410     0.410       0.410      0.410
23   2.3  0.570     0.570     0.570       0.570      0.570
24   2.4  0.380     0.380     0.380       0.380      0.380
25   2.5  0.450     0.450     0.450       0.450      0.450
26   2.6  0.600     0.600     0.600       0.600      0.600
27   2.7  0.350     0.350     0.350       0.350      0.350
28   2.8  0.700     0.700     0.700       0.700      0.700
29   2.9  0.670     0.670     0.670       0.670      0.670
30   3.0  0.765     0.765     0.765       0.765      0.765
> print(rbf_metrics)
   sigma recall precision f_measure specificity rand_index
1    0.1  0.040     0.040     0.040       0.040      0.040
2    0.2  0.240     0.240     0.240       0.240      0.240
3    0.3  0.790     0.790     0.790       0.790      0.790
4    0.4  0.255     0.255     0.255       0.255      0.255
5    0.5  0.620     0.620     0.620       0.620      0.620
6    0.6  0.840     0.840     0.840       0.840      0.840
7    0.7  0.135     0.135     0.135       0.135      0.135
8    0.8  0.505     0.505     0.505       0.505      0.505
9    0.9  0.650     0.650     0.650       0.650      0.650
10   1.0  0.550     0.550     0.550       0.550      0.550
11   1.1  0.515     0.515     0.515       0.515      0.515
12   1.2  0.540     0.540     0.540       0.540      0.540
13   1.3  0.455     0.455     0.455       0.455      0.455
14   1.4  0.545     0.545     0.545       0.545      0.545
15   1.5  0.525     0.525     0.525       0.525      0.525
16   1.6  0.470     0.470     0.470       0.470      0.470
17   1.7  0.620     0.620     0.620       0.620      0.620
18   1.8  0.420     0.420     0.420       0.420      0.420
19   1.9  0.600     0.600     0.600       0.600      0.600
20   2.0  0.535     0.535     0.535       0.535      0.535
21   2.1  0.515     0.515     0.515       0.515      0.515
22   2.2  0.445     0.445     0.445       0.445      0.445
23   2.3  0.530     0.530     0.530       0.530      0.530
24   2.4  0.470     0.470     0.470       0.470      0.470
25   2.5  0.535     0.535     0.535       0.535      0.535
26   2.6  0.510     0.510     0.510       0.510      0.510
27   2.7  0.635     0.635     0.635       0.635      0.635
28   2.8  0.495     0.495     0.495       0.495      0.495
29   2.9  0.590     0.590     0.590       0.590      0.590
30   3.0  0.580     0.580     0.580       0.580      0.580
> 
> PlotSigmaMetrics(laplace_metrics)
> PlotSigmaMetrics(anova_metrics)
> PlotSigmaMetrics(rbf_metrics)
> 
> GetKCrossValidatedSVMMetrics <- function(df, folds = 10, kernel = "vanilladot", 
+                                          sigma_values)
+ {
+   res <- matrix(nrow = 0, ncol = 8)
+   
+   for(sigma_value in sigma_values)
+   {
+     metrics_list <- GetCrossValidatedSVMMetrics(df, folds, kernel = kernel, 
+                                                 cost = 1, sigma = sigma_value)
+     res <- rbind(res, c(sigma_value, 
+                       as.numeric(unlist(metrics_list["recall"])), 
+                       as.numeric(unlist(metrics_list["precision"])), 
+                       as.numeric(unlist(metrics_list["f_measure"])), 
+                       as.numeric(unlist(metrics_list["specificity"])),
+                       as.numeric(unlist(metrics_list["rand_index"])),
+                       as.numeric(unlist(metrics_list["cross_error"])),
+                       as.numeric(unlist(metrics_list["fraction(support_vectors)"]))))
+   }
+   
+   colnames(res) <- c("sigma", "recall", "precision", "f_measure", "specificity",
+                      "rand_index", "cross_error", "fraction(support_vectors)")
+   return(as.data.frame(res))
+ }
> 
> sigma_values <- seq(from=1.0, to=2.0, by=0.1)
> laplace_metrics <- GetKCrossValidatedSVMMetrics(df, kernel = "laplacedot", 
+                                                 sigma_values = sigma_values)
> anova_metrics <- GetKCrossValidatedSVMMetrics(df, kernel = "anovadot", 
+                                               sigma_values = sigma_values)
> rbf_metrics <- GetKCrossValidatedSVMMetrics(df, kernel = "rbfdot", 
+                                             sigma_values = sigma_values)
> 
> print(laplace_metrics)
   sigma recall precision f_measure specificity rand_index  cross_error fraction(support_vectors)
1    1.0  1.000     1.000     1.000       1.000      1.000 0.0001111111                 0.9972222
2    1.1  1.000     1.000     1.000       1.000      1.000 0.0001111111                 0.9987778
3    1.2  1.000     1.000     1.000       1.000      1.000 0.0000000000                 0.9995556
4    1.3  1.000     1.000     1.000       1.000      1.000 0.0003333333                 1.0000000
5    1.4  1.000     1.000     1.000       1.000      1.000 0.0010000000                 1.0000000
6    1.5  1.000     1.000     1.000       1.000      1.000 0.0006666667                 1.0000000
7    1.6  0.999     0.999     0.999       0.999      0.999 0.0020000000                 1.0000000
8    1.7  0.996     0.996     0.996       0.996      0.996 0.0017777778                 1.0000000
9    1.8  0.998     0.998     0.998       0.998      0.998 0.0024444444                 1.0000000
10   1.9  1.000     1.000     1.000       1.000      1.000 0.0044444444                 1.0000000
11   2.0  0.993     0.993     0.993       0.993      0.993 0.0066666667                 1.0000000
> print(anova_metrics)
   sigma recall precision f_measure specificity rand_index  cross_error fraction(support_vectors)
1    1.0  1.000     1.000     1.000       1.000      1.000 0.0008888889                 0.2693333
2    1.1  1.000     1.000     1.000       1.000      1.000 0.0020000000                 0.2793333
3    1.2  1.000     1.000     1.000       1.000      1.000 0.0017777778                 0.2824444
4    1.3  0.998     0.998     0.998       0.998      0.998 0.0017777778                 0.2936667
5    1.4  1.000     1.000     1.000       1.000      1.000 0.0021111111                 0.3002222
6    1.5  0.998     0.998     0.998       0.998      0.998 0.0018888889                 0.3064444
7    1.6  0.997     0.997     0.997       0.997      0.997 0.0023333333                 0.3110000
8    1.7  0.999     0.999     0.999       0.999      0.999 0.0028888889                 0.3123333
9    1.8  0.998     0.998     0.998       0.998      0.998 0.0032222222                 0.3208889
10   1.9  0.998     0.998     0.998       0.998      0.998 0.0033333333                 0.3240000
11   2.0  0.999     0.999     0.999       0.999      0.999 0.0027777778                 0.3272222
> print(rbf_metrics)
   sigma recall precision f_measure specificity rand_index cross_error fraction(support_vectors)
1    1.0  0.999     0.999     0.999       0.999      0.999 0.001222222                 0.7882222
2    1.1  0.999     0.999     0.999       0.999      0.999 0.002000000                 0.8024444
3    1.2  0.995     0.995     0.995       0.995      0.995 0.004333333                 0.8130000
4    1.3  1.000     1.000     1.000       1.000      1.000 0.004555556                 0.8212222
5    1.4  0.997     0.997     0.997       0.997      0.997 0.008000000                 0.8320000
6    1.5  0.989     0.989     0.989       0.989      0.989 0.010111111                 0.8411111
7    1.6  0.993     0.993     0.993       0.993      0.993 0.013222222                 0.8486667
8    1.7  0.986     0.986     0.986       0.986      0.986 0.017888889                 0.8574444
9    1.8  0.981     0.981     0.981       0.981      0.981 0.018888889                 0.8671111
10   1.9  0.980     0.980     0.980       0.980      0.980 0.025000000                 0.8727778
11   2.0  0.977     0.977     0.977       0.977      0.977 0.027444444                 0.8867778
> 
> PlotSigmaMetrics(laplace_metrics)
> PlotSigmaMetrics(anova_metrics)
> PlotSigmaMetrics(rbf_metrics)
> 
> PlotKernelEigen <- function(k)
+ {
+   eig_k <- eig(k)
+   
+   print("Sum of top m eigen values : ")
+   print(sum(eig_k))
+   
+   eig_k_df <- cbind(seq(length(eig_k)), as.data.frame(eig_k))
+   colnames(eig_k_df) <- c("i", "value")
+   ggplot(eig_k_df, aes(x = i, y = value)) + 
+     geom_point(size=2) +     
+     geom_line() +
+     theme(legend.position="none") +
+     xlab("Component") + 
+     ylab("Eigen value")
+ }
> 
> PlotKPCA <- function(df, kernel = "vanilladot", sigma = 0.15)
+ {
+   test_size <- round(nrow(df)/10)
+   test_index <- sample(nrow(df), test_size)
+   class_index <- which(colnames(df) == "class")  
+   
+   k <- kpca(as.matrix(df[-test_index, -class_index]), kernel = kernel, 
+             kpar = list(sigma = sigma))
+   
+   PlotKernelEigen(k)
+   
+   rot_k_df <- as.data.frame(cbind(rotated(k)[,1:2], df[-test_index, class_index]))
+   colnames(rot_k_df) <- c("x", "y", "class")
+   
+   emb <- predict(k, df[test_index, -class_index])
+   emb_df <- cbind(as.data.frame(emb[,1:2]), df[test_index, class_index])
+   colnames(emb_df) <- c("x", "y", "class")
+   
+   ggplot(rot_k_df, aes(x = x, y = y, col = ifelse(class == 1, "red", "blue"))) +
+     geom_point(emb_df, mapping = aes(x = x, y = y, shape = "2", col = ifelse(class == 1, "red", "blue"))) +
+     geom_point(size=3) +
+     theme(legend.position="none") +
+     xlab("1st Principal Component") + 
+     ylab("2nd Principal Component")
+ }
> 
> PlotKPCA(df, kernel = "rbfdot", sigma = 0.15)
[1] "Sum of top m eigen values : "
[1] 0.9109477
> PlotKPCA(df, kernel = "laplacedot", sigma = 0.5)
[1] "Sum of top m eigen values : "
[1] 0.9192155
> 
> # ------ Exercise 5 ------ 
> 
> GetHighDimDataset <- function(df)
+ {
+   high_dim_df <- matrix(ncol = ncol(df) + length(seq(2,5)) * 2, nrow = 0)
+   for(i in seq(1, nrow(df)))
+   {
+     new_values <- c()
+     for(j in seq(2, 5))
+     {
+       new_values <- c(new_values, df[i,1]^j, df[i,2]^j)
+     }
+   
+     new_values <- matrix(new_values, nrow = 1, ncol = length(new_values))
+     high_dim_df <- rbind(high_dim_df, cbind(df[i,], new_values))
+   }
+   
+   return(as.data.frame(high_dim_df))
+ }
> 
> ApplyKPCA_DimReduction <- function(high_dim_df, kernel = "rbfdot", 
+                                    sigma = 0.000001, num_features = 6)
+ {
+   folds <- 10
+   class_index <- which(colnames(high_dim_df) == "class")  
+   test_indices <- split(sample(nrow(high_dim_df)), 1:folds)
+   confusion_matrix <- matrix(0, ncol = 2, nrow = 2)
+   
+   set.seed(100)
+   for(test_index in test_indices)
+   {
+     test_index = test_indices[1]
+     test_index <- as.numeric(unlist(test_index))
+         
+     kpca_res <- kpca(as.matrix(high_dim_df[-test_index, -class_index]), 
+                      kernel = kernel, 
+                      kpar = list(sigma = sigma), features = num_features)
+     
+     PlotKernelEigen(kpca_res)
+     
+     new_pts <- cbind(rotated(kpca_res), class = high_dim_df[-test_index, 
+                                                             class_index])
+     new_class_index <- which(colnames(new_pts) == "class")  
+     
+     kmean_res <- kmeans(new_pts[,-new_class_index], 2)
+     found_df <- as.data.frame(cbind(new_pts[,-new_class_index], 
+                                     class = kmean_res$cluster))
+     confusion_matrix <- confusion_matrix + GetConfusionMatrix(found_df= found_df, 
+                                          ground_truth_df = high_dim_df[-test_index,])
+   }
+   
+   confusion_matrix <- confusion_matrix/folds
+   metrics <- GetPerformanceMetrics(confusion_matrix)
+   PrintPerformanceMetrics(metrics)
+ }
> 
> high_dim_df <- GetHighDimDataset(df)
> class_index <- which(colnames(high_dim_df) == "class")
> 
> kmeans_df <- GetKMeansClusters(high_dim_df[,-class_index])
> confusion_matrix <- GetConfusionMatrix(found_df=kmeans_df, ground_truth_df=df)
> metrics_list <- GetPerformanceMetrics(confusion_matrix)
> PrintPerformanceMetrics(metrics_list)
[1] "Recall : "
[1] 0.595
[1] "Precision : "
[1] 0.595
[1] "F Measure : "
[1] 0.595
[1] "Specificity : "
[1] 0.595
[1] "RAND Index : "
[1] 0.595
> 
> ApplyKPCA_DimReduction(high_dim_df)
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Sum of top m eigen values : "
[1] 0.303693
[1] "Recall : "
[1] 0.6866667
[1] "Precision : "
[1] 0.6866667
[1] "F Measure : "
[1] 0.6866667
[1] "Specificity : "
[1] 0.6866667
[1] "RAND Index : "
[1] 0.6866667
