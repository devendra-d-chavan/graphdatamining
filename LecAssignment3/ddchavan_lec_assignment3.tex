\documentclass[a4paper,fleqn]{article}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{amsthm}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{minted}
\usepackage{caption}

\usepackage[xetex]{graphicx} 
\usepackage{fontspec,xunicode} 
\usepackage[pdfauthor={Devendra D. Chavan}, 
                        pdftitle={Lecture Assignment 3 - CSC591 - Graph Data 
                                    Mining}, 
                        pdfsubject={Lecture Assignment 3}, 
                        pdfkeywords={CSC591, NCSU, Graph Data Mining, 
                                        Lecture Assignment}, 
                        pdfproducer={XeLateX with hyperref} 
                        pdfcreator={Xelatex}]{hyperref} 
                
\begin{document}
\section*{\centering Graph Data Mining - Lecture Assignment 3\\
                Kernelization and kernel tricks}
\vspace{10 pt}
\textbf{Name : Devendra D. Chavan}\\
\textbf{Unity Id : ddchavan}

\section{Exercise 1}
\begin{enumerate}[(a)]
    \item
    {
        The Gaussian kernel is defined as \\
        \[
            s(x_i, x_j) = e^{{||x_i - x_j||}^2/2\sigma^2}
        \]
        A function is a valid kernel function if it satisfies the criteria
        that it is symmetric and positive semi-definite.\\
        The numerator of the power ${||x_i - x_j||}^2$ is defined as 
        $\sqrt{(x_{i1} - x_{j1})^2 + (x_{i2} - x_{j2})^2 + ... + (x_{in} - x_{jn})^2}$,
        which is symmetric (since each component's value is squared). 
    }
    \item
    {
        If the distance function is denoted by $d(x_i, x_j)$ in the original
        space then,\\
        \[
            d^2(x_i, x_j) = ||x_i - x_j||^2
        \]
        Applying the Gaussian kernel function $K(x_i, x_j)$,\\
        \[
            \begin{array}{lcl}
            d^2(\Phi(x_i), \Phi(x_j)) &=& ||\Phi(x_i) - \Phi(x_j)||^2\\
                                      &=& <\Phi(x_i) - \Phi(x_j), \Phi(x_i) - \Phi(x_j)>\\
                                      &=& <\Phi(x_i), \Phi(x_i)> - 2<\Phi(x_i), \Phi(x_j)> 
                                            + <\Phi(x_j), \Phi(x_j)>\\
                                      &=& = K(x_i, x_i) - 2K(x_i, x_j) + K(x_j, x_j)
            \end{array}                                      
        \]
        Now, $K(x_i, x_i) = 1$, therefore the above equation reduces to \\
        $ d^2(\Phi(x_i), \Phi(x_j)) = 2 - 2K(x_i, x_j)$
    }
    \item
    {
        When the points lie on a sphere of radius $1$, then the value of $\sigma$
        in the denominator of the Gaussian function exponent is $1$. Thus the
        function simplifies to \\
        \[
            \begin{array}{lcl}
                d^2(\Phi(x_i), \Phi(x_j)) &=& 2 - 2K(x_i, x_j)\\
                                          &=& 2 - 2e^{{||x_i - x_j||}^2/2*1^2}\\
                                          &=& 2 - 2e^{{||x_i - x_j||}^2/2}
            \end{array}
        \]
    }
\end{enumerate}

\clearpage
\section{Generating the datasets}
A non linear dataset in two dimensions will cause all the three methods to 
perform poorly.
\begin{figure}[H]                                                           
    \centering                                                              
    \includegraphics[width=150mm]{Graphs/2.pdf}                      
    \caption{Non linear dataset in 2 dimensions}                               
    \label{fig:2}                                                        
\end{figure}                   

\clearpage
\section{Evaluating the datasets}
The recall, precision, f measure, specificity and RAND index metrics are 
evaluated for the kmeans and svm clustering methods.

\noindent
\textbf{k means metrics}
\inputminted[firstline=127, lastline= 137]{r}{"ddchavan_lec_assignment3.R.output"}

\noindent
\textbf{svm metrics with 10 fold cross validation}
\inputminted[firstline=255, lastline=274]{r}{"ddchavan_lec_assignment3.R.output"}

\noindent
For the \textbf{PCA evaluation}, the projection on the principal component 
showed that the data points overlap.
\begin{figure}[H]                                                           
    \centering                                                              
    \includegraphics[width=150mm]{Graphs/3_pca_projection.pdf}                      
    \caption{Projected data points}                               
    \label{fig:3a}                                                        
\end{figure}                   
\begin{figure}[H]                                                           
    \centering                                                              
    \includegraphics[width=150mm]{Graphs/3_pca_variances.pdf}                      
    \caption{PCA variances}                               
    \label{fig:3b}                                                        
\end{figure}                   

\clearpage
\section{Kernelizing the methods}
\begin{enumerate}[(a)]
    \item
    {
        The kernel methods \textit{laplacedot - Laplacian kernel, 
        anovadot - ANOVA RBF kernel and rbfdot Radial Basis kernel "Gaussian"} 
        were chosen.
    }
    \item
    {
        The metrics were plotted for different values of sigma. The metrics for 
        showed an improvement over the non kernelized approach (see (d) for 
        plots).
        
        \noindent
        \textbf{Original k means metrics}
        \inputminted[firstline=127, lastline=136]{r}{"ddchavan_lec_assignment3.R.output"}

        \noindent
        \textbf{Laplacian kernel k means metrics}
        \inputminted[firstline=364, lastline=395]{r}{"ddchavan_lec_assignment3.R.output"}
        
        \noindent
        \textbf{ANOVA kernel k means metrics}
        \inputminted[firstline=396, lastline=427]{r}{"ddchavan_lec_assignment3.R.output"}

        \noindent
        \textbf{Radial "Gaussian" kernel k means metrics}
        \inputminted[firstline=428, lastline=460]{r}{"ddchavan_lec_assignment3.R.output"}
        
        \noindent
        \textbf{Original svm metrics}
        \inputminted[firstline=254, lastline=273]{r}{"ddchavan_lec_assignment3.R.output"}

        \noindent
        \textbf{Laplacian kernel svm metrics}
        \inputminted[firstline=497, lastline=509]{r}{"ddchavan_lec_assignment3.R.output"}

        \noindent
        \textbf{ANOVA kernel svm metrics}
        \inputminted[firstline=510, lastline=522]{r}{"ddchavan_lec_assignment3.R.output"}

        \noindent
        \textbf{Radial "Gaussian" svm metrics}
        \inputminted[firstline=523, lastline=535]{r}{"ddchavan_lec_assignment3.R.output"}
    }
    \item
    {
        Yes, a performance difference is observed.
    }
    \item
    {
        The \textit{sigma} parameter was varied for kkmeans and ksvm methods.
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_1_a.pdf}                      
            \caption{Kmeans - Laplacian kernel}                               
            \label{fig:3b}                                                        
        \end{figure}                   
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_1_b.pdf}                      
            \caption{Kmeans - ANOVA kernel}                               
            \label{fig:3b}                                                        
        \end{figure}                   
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_1_c.pdf}                      
            \caption{Kmeans - Gaussian kernel}                               
            \label{fig:3b}                                                        
        \end{figure}                   
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_2_a.pdf}                      
            \caption{Kmeans - Laplacian kernel}                               
            \label{fig:3b}                                                        
        \end{figure}                   
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_2_b.pdf}                      
            \caption{Kmeans - ANOVA kernel}                               
            \label{fig:3b}                                                        
        \end{figure}                   
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_2_c.pdf}                      
            \caption{Kmeans - Gaussian kernel}                               
            \label{fig:3b}                                                        
        \end{figure}                   
        
        \noindent
        The projection using kpca is plotted to evaluate kpca
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/3_pca_projection.pdf}                      
            \caption{PCA - Original}                       
            \label{fig:3b}                                                        
        \end{figure}                   
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_3_a.pdf}                      
            \caption{KPCA - Gaussian kernel}                               
            \label{fig:3b}                                                        
        \end{figure}                   
        \begin{figure}[H]                                                           
            \centering                                                              
            \includegraphics[width=150mm]{Graphs/4_3_b.pdf}                      
            \caption{KPCA - Laplacian kernel }                               
            \label{fig:3b}                                                        
        \end{figure}                           
    }
\end{enumerate}

\clearpage
\section{Pipelining}
\begin{enumerate}[(a)]
    \item
    {
        The dataset is extended to 10 dimensions.
    }
    \item
    {
        The results of the k means clustering on the high dimension dataset
        shows poor performance.\\

        \inputminted[firstline=653, lastline=663]{r}{"ddchavan_lec_assignment3.R.output"}
    }
    \item
    {
        The first $6$ vectors are chosen based on the eigen value distribution.
        The first 6 eigen vectors preserve approximately $30\%$ variability. 
        This is determined by the sum of the eigen values which is $0.30363$
    }
    \item
    {
        The performance metrics after running k means shows an improvement in 
        the results

        \noindent
        \textbf{High dimension kmeans results}
        \inputminted[firstline=653, lastline=663]{r}{"ddchavan_lec_assignment3.R.output"}

        \noindent
        \textbf{Lower dimension kmeans results}
        \inputminted[firstline=665, lastline=695]{r}{"ddchavan_lec_assignment3.R.output"}
    }
    \item
    {
        The kernel function transforms the data into a lower dimension while
        maintaining the variability of the data. Thus allowing the usage of
        k means on the lower dimension data with better performance results.
    }
\end{enumerate}

\clearpage
\section{Appendix}
\subsection*{R script - ddchavan\_lec\_assignment3.R output}
\inputminted{r}{"ddchavan_lec_assignment3.R.output"}
\end{document}
