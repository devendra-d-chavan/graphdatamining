\documentclass[a4paper,fleqn]{article}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{amsthm}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{minted}
\usepackage{caption}

\usepackage[xetex]{graphicx} 
\usepackage{fontspec,xunicode} 
\usepackage[pdfauthor={Devendra D. Chavan}, 
                        pdftitle={Homework 1 - CSC591 - Graph Data Mining}, 
                        pdfsubject={Homework 1}, 
                        pdfkeywords={CSC591, NCSU, Graph Data Mining, Homework}, 
                        pdfproducer={XeLateX with hyperref} 
                        pdfcreator={Xelatex}]{hyperref} 
                
\begin{document}
\section*{}
\textbf{Name : Devendra D. Chavan}\\
\textbf{Unity Id : ddchavan}

\section*{Problem 1}
\subsection*{1. Questions to ask}
\begin{enumerate}[a)]
    \item
    {
        Is the graph directed or undirected? : This can potentially reduce 
        the space needed to store the graph in an adjacency matrix 
        representation by half (as the matrix is symmetric in case of an 
        undirected graph).
    }
    \item
    {
        Is the graph sparse or dense? : This can help in deciding whether the 
        graph can be represented as an adjacency matrix (in case the graph is 
        a dense graph) or an adjacency list (in case the graph is a sparse 
        graph), which affect the space and time complexity of the proposed
        algorithm.
    }
    \item
    {
        Is the graph labelled or unlabeled? If labelled, then what is the type
        of data as a label in the graph? Are both the vertices and edges 
        labelled? : This affects the space requirement of the graph. 
    }
    \item
    {
        Is the graph weighted or unweighted? : In case the graph is weighted, 
        then the proposed algorithms would need to consider these weights
        while determined some properties of a graph, for example, the shortest
        path between two vertices ($u,v$) would be vary if the edges have
        weights. Also, additional space would be needed to store the 
        edge weights.
    }
\end{enumerate}

\subsection*{2. Scenarios}
\begin{enumerate}[a)]
    \item { Undirected, dense, unlabeled, unweighted graph }
    \item { Undirected, sparse, unlabeled, weighted graph }
    \item { Directed, sparse, unlabeled, unweighted graph }
\end{enumerate}

\subsection*{3. Proposed data structures}
\begin{enumerate}[a)]
    \item
    {
        \textbf{Undirected, dense, unlabeled, unweighted graph}
        In this case, the graph would be most efficiently represented as an 
        adjacency matrix. Furthermore, since the graph is undirected, the 
        the matrix representation would also be symmetric.\\
        Adding and removing a vertex would involve changing the space allocated 
        to the graph and re-copying all the values to the new matrix i.e. 
        $O(n^2)$. Adding or removing an edge on the other hand would involve
        only changing the value for that edge $(u,v)$ in the matrix i.e. 
        $O(1)$.\\
        In terms of the space requirement, since the graph is unweighted and 
        unlabeled, the only information that would be needed to be 
        represented in the graph is whether the edge is connected or not i.e.
        a boolean value requiring a single bit for storage. Also, the symmetric
        property of the graph would mean that only half the number of nodes 
        would actually require storage. Thus in terms of the space complexity,
        the representation would require 
        $\frac{\displaystyle n^2}{\displaystyle 2 * 8}$ bytes for storing $n$ 
        vertices i.e. $O(n^2)$.
    }
    \item
    {
        \textbf{Undirected, sparse, unlabeled, weighted graph}
        In this, the graph would be efficiently represented as an adjacency 
        list. \\
        Adding a vertex would involve adding the vertex to the list of vertices
        with an empty edge list i.e. $O(1)$. While removing a vertex would involve removing 
        all the edges associated with that vertex i.e. $O(deg(v))$ which is 
        $O(n-1)$ in the worst case (complete graph, which would also mean a 
        dense graph).\\
        Adding an edge would involve adding an edge to the list of edges 
        associated with that vertex i.e. $O(1)$, while removing an edge
        would involve removing the edge from the list of edges at a vertex i.e.
        $O(1)$.\\
        In terms of the space requirement, the adjacency list representation
        would require $O(n+m)$ storage where $n$ is the number of vertices in 
        the graph and $m$ is the number of edges in the graph. As the graph is
        weighted in this case, the weights for each edge would also require 
        additional storage (though this could be reduced by applying an 
        encoding scheme or compressing the values of the weights).
    }
    \item
    {
        \textbf{Directed, sparse, unlabeled, unweighted graph}
        In this case, the graph would still be represented as an adjacency 
        list.\\
        Adding and removing a vertex would still have the same time complexity
        as in the earlier case. In addition, the time complexity of removing an
        edge or a vertex would also remain the same as in earlier case.\\
        In terms of the space complexity, since the adjacency matrix would 
        require $O(n+m)$ storage as earlier, but since the edges are 
        unweighted, each edge would require only a since bit for storing 
        whether it is present or not. The extra storage required for storing
        the weight is not needed. Therefore, the number of bytes required to 
        store the $m$ edges would be $\frac{\displaystyle m}{\displaystyle 8}$.
    }
\end{enumerate}

\section*{Problem 2}
\subsection*{1. Warshall algorithm to compute the path matrix}
The Warshall algorithm can be used to compute the path matrix given an
adjacency matrix. The basic premise of the algorithm is to compute all paths 
containing between the two vertices $(i,j) \in V$ and containing an 
intermediate vertex in the set $\{1,2,..,k\}$. A sequence of matrices $P^{(0)},
P^{(1)},..,P^{(n)}$ such that $P^{(0)} = A \text{ and } P^{(n)} = P$, the path 
matrix and $P^{(i)}$ containing all paths with intermediate nodes from the set
of vertices $\{1,2,..,i\}$.\\
The algorithm can be stated as below\\
Let $P^{(0)} = A$ (the adjacency matrix)\\
Let $P^{(r)}$ is a matrix such that 
\[
    p_{ik}^{(r)} = \left\{
                    \begin{array}{ll}
                        1 & \text{ iff there is a path connecting nodes $i$ 
                            and $k$ through one or more of nodes 
                            $\{1, 2,..., r\}$}\\
                        0 & \text{ otherwise}
                    \end{array}
                \right.
\]\\
Let $P^{(r+1)}$ is the matrix where 
\[
    p_{ik}^{(r+1)} = \left\{
                        \begin{array}{ll}
                        1 & \text{ iff there is a path connecting nodes $i$ 
                        and $k$ through one or more of nodes 
                        $\{1, 2, ..., r, r+1\}$}\\
                        0 & \text{ otherwise}
                        \end{array}
                    \right.
\]
Now, $P^{(r+1)}$ can be calculated from $P^{(r)}$ as follows\\
If $p_{ik}^{(r)} = 1$ then $p_{ik}^{(r+1)} := 1$\\
Else $p_{ik}^{(r+1)} := p_{i(r+1)}^{(r)} * p_{(r+1)k}^{(r)}$\\
Thus, $p_{ik}^{(r+1)} = 1$ iff there is a path connecting nodes $i$ and $k$ 
and containing intermediate nodes selected from the set 
$\{1, 2, 3, ..., r+1\}$ and zero otherwise.

\subsection*{2. Pseudocode}
\begin{algorithm}
    \label{alg:PathMatrix}
    \caption{Calculate the path matrix $P$ of a graph represented as an 
    adjacency matrix $A$, $CalculatePathMatrix(A)$}
    \begin{algorithmic}
    \For{$i = 1 \to A.length$}
        \For{$j = 1 \to A.length$}
            \State{$P[i][j] \gets A[i][j]$}
        \EndFor
    \EndFor
    \For{$k = 1 \to A.length$}
        \For{$i = 1 \to A.length$}
            \For{$j = 1 \to A.length$}
                \If {$P[i][j] == False$}
                    \State{$P[i][j] \gets (P[i][j] \>||\> 
                        (P[i][k] \> \&\& \> P[k][j]))$}
                \EndIf
            \EndFor
        \EndFor
    \EndFor
    \end{algorithmic}
\end{algorithm}

\subsection*{3. Time complexity}
The calculation of the path matrix $P$ involves iterating from $1 \to 
A.length$, where $A.length = |V|$ in three nested for loops. Therefore, the
worst case time complexity of the algorithm is $O(|V|^3)$.

\subsection*{4. Maximum value of N}  
The algorithm performs iterations from $1 \to A.length$ in several for loops, 
each of which have an increment operation. In addition, the second for loop 
also has logical operations and conditional check.
Therefore, the number of operations in terms on $N$ can be summarized as:\\
\begin{enumerate}
    \item
    {
        \textbf{First for loop block}: $N^2$ increment operations
    }
    \item
    {
        \textbf{Second for loop block}: $N^3$ increment operations, $(2N)^3$ 
        logical operations and $N^3$ comparison operations i.e. which in the 
        worst case would amount to $10N^3$ operations.
    }
\end{enumerate}
Therefore, the algorithm requires $10N^3 + N^2$ operations which can be 
approximated to the dominant term $10N^3$\\
For a machine with $2.4*10^9$ FLOPs, the value of $N$ for which the time taken 
would be one day would be 
$\sqrt[3]{\frac{\displaystyle (24*60*60)*(2.4*10^9)}{\displaystyle 10}} = 
27473$. Thus in one day a graph with $27473$ can be processed.


\subsection*{5. Space requirement}
The algorithm requires $2*|V|^2$ number of bits, one matrix containing the 
adjacency matrix while the other contains the path matrix. Both the matrices 
require only a bit for storing the value for each $(i,j)$. Therefore, for a 
512 MB machine ($=512*10^6*8 bits = 4096*10^6 bits$) the maximum number of 
vertices would be $\sqrt{\frac{\displaystyle 4096*10^6}{\displaystyle 2}} = 
45254$. This is greater than the maximum number of nodes that can be 
processed in one day. \\
A machine with 1 GB of RAM ($=10^9*8 bits$) can handle 
$\sqrt{\frac{\displaystyle 8*10^9}{\displaystyle 2}} = 89442$ vertices.\\
In case the laptop has 16 GB of RAM ($=16*10^9*8 bits = 128*10^9 bits$) then it
can handle $\sqrt{\frac{\displaystyle 128*10^9}{\displaystyle 2}} = 252982$ 
vertices.
\end{document}
